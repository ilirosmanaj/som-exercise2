#!/usr/bin/env bash
export PYTHONPATH=.
# install requirements
python3.7 -m pip install --user -r requirements.txt

# repeat 10 times with given params
python3.7 usecases/genetic_algorithms/genetic_algorithms.py -p tsp -r 10
