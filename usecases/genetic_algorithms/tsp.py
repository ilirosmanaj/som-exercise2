import random
from typing import List


class TourManager:
    def __init__(self):
        self.destination_nodes = []

    def add_node(self, node):
        self.destination_nodes.append(node)

    def get_node(self, index):
        return self.destination_nodes[index]

    @property
    def number_of_nodes(self):
        return len(self.destination_nodes)


class Tour:
    def __init__(self, tour_manager, tour=None):
        self.tour_manager = tour_manager
        self.tour = tour if tour else [None for i in range(0, self.tour_manager.number_of_nodes)]
        self.distance = 0

    def __len__(self):
        return len(self.tour)

    def __getitem__(self, index):
        return self.tour[index]

    def __setitem__(self, key, value):
        self.tour[key] = value

    def __repr__(self):
        return ' '.join([t.name for t in self.tour])

    def generate_individual(self):
        for node_index in range(0, self.tour_manager.number_of_nodes):
            self.set_node(node_index, self.tour_manager.get_node(node_index))
        random.shuffle(self.tour)

    def get_node(self, tour_position):
        return self.tour[tour_position]

    def set_node(self, tour_position, node):
        self.tour[tour_position] = node

        # reset distances so that they will be calculated on next fetch
        self.distance = 0

    def get_fitness(self):
        return abs(self.get_distance())

    def get_distance(self):
        if self.distance == 0:
            tour_distance = 0
            for node_index, node in enumerate(self.tour):
                destination_node = self.tour[(node_index + 1) % self.tour_size]
                tour_distance += node.distance_to(destination_node)

            self.distance = tour_distance

        return self.distance

    @property
    def tour_size(self):
        return len(self.tour)

    def contains_node(self, node):
        return node in self.tour


class PopulationTSP:
    def __init__(self, tour_manager, population_size: int, initialise: bool):
        self.tours: List[Tour] = [None for i in range(0, population_size)]

        if initialise:
            for i, tour in enumerate(self.tours):
                tour = Tour(tour_manager)
                tour.generate_individual()
                self.save_tour(i, tour)

    def __setitem__(self, key, value):
        self.tours[key] = value

    def __getitem__(self, index):
        return self.tours[index]

    def save_tour(self, index, tour):
        self.tours[index] = tour

    def get_tour(self, index) -> Tour:
        return self.tours[index]

    def get_fittest(self):
        """Returns the tour with least fitness value"""
        return sorted(self.tours, key=lambda t: t.get_fitness())[0]

    def get_sorted(self):
        """Returns the tour with least fitness value"""
        return sorted(self.tours, key=lambda t: t.get_fitness())

    @property
    def population_size(self):
        return len(self.tours)


class GeneticAlgorithmTsp:
    def __init__(self, tour_manager, mutation_rate: float, tournament_size: int):
        self.tour_manager = tour_manager
        self.mutation_rate = mutation_rate
        self.tournament_size = tournament_size
        self.best_so_far = None

    def evolve_population(self, pop):
        new_population = PopulationTSP(self.tour_manager, pop.population_size, False)

        for i in range(0, new_population.population_size):
            parent_1, parent_2 = self.tournament_selection(pop)
            child = self.crossover(parent_1, parent_2)

            # mutate the population (if luck decides to...)
            self.mutate(child)

            new_population.save_tour(i, child)

        fittest = new_population.get_fittest()
        if not self.best_so_far:
            self.best_so_far = fittest

        if self.best_so_far.get_fitness() > fittest.get_fitness():
            self.best_so_far = fittest
        return new_population

    def crossover(self, parent1, parent2):
        child = Tour(self.tour_manager)

        start_pos = int(random.random() * parent1.tour_size)
        end_pos = int(random.random() * parent1.tour_size)

        for i in range(0, child.tour_size):
            if start_pos < end_pos and start_pos < i < end_pos:
                child.set_node(i, parent1.get_node(i))
            elif start_pos > end_pos:
                if not end_pos < i < start_pos:
                    child.set_node(i, parent1.get_node(i))

        for i in range(0, parent2.tour_size):
            if not child.contains_node(parent2.get_node(i)):
                for j in range(0, child.tour_size):
                    if not child.get_node(j):
                        child.set_node(j, parent2.get_node(i))
                        break
        return child

    def mutate(self, tour):
        if random.random() < self.mutation_rate:
            tour_pos1, tour_pos2 = 1, 1
            while tour_pos1 == tour_pos2:
                tour_pos1 = int(tour.tour_size * random.random())
                tour_pos2 = int(tour.tour_size * random.random())

            node1 = tour.get_node(tour_pos1)
            node2 = tour.get_node(tour_pos2)

            tour.set_node(tour_pos2, node1)
            tour.set_node(tour_pos1, node2)

    def tournament_selection(self, pop):
        """Returns two parents from a randomly chosen population"""
        tournament = PopulationTSP(self.tour_manager, self.tournament_size, False)
        for i in range(0, self.tournament_size):
            random_id = int(random.random() * pop.population_size)
            tournament.save_tour(i, pop.get_tour(random_id))

        fittest = tournament.get_sorted()

        parent1 = fittest[0]
        parent2_index = 1
        parent2 = fittest[parent2_index]

        while parent1 == parent2:
            try:
                parent2_index += 1
                parent2 = fittest[parent2_index]
            except IndexError:
                pass

        return parent1, parent2
