# som-exercise2

Exercise 2 for Self Organizing Systems course. Implement Genetic Algorithms and Ant Colony Optimization for graph coloring and cost balancing traveling salesman person.

## General description

Both of the problems are NP-hard problems. Which means finding the optimal solution
for them without enumeration is impossible.


### Cost balancing traveling salesperson problem

#### Problem Description

We consider the Cost-Balanced Traveling Salesperson Problem† (CBTSP), an adaption
of the classical TSP. Given an undirected, simple, connected graph G = (V,E) with
edge costs di,j ∈ Z, ∀(i, j) ∈ E, find a Hamiltonian cycle C ⊂ E with cost closest
to 0, i.e., it minimizes the sum of all edges in a cycle C.

The main difference to the original TSP is that edge costs can also be negative and therefore
also the cost of a Hamiltonian cycle.

#### Instances

All test instances are downloaded from https://www.ants-lab.it/mess2018/ and are
stored under instances directory.

Size of the graph starts from 10 nodes to 3000 thousand. This can be used to compare
performance on small versus large instances.

The format of the instances is like the following:

```bash
<n> <m>
<vertex_1_of_edge_1> <vertex_2_of_edge_1> <cost_of_edge_1>
...
<vertex_1_of_edge_m> <vertex_2_of_edge_m> <cost_of_edge_m>
```

Where n and m is the of vertices and edges respectively.

### Graph coloring problem

#### Problem Description

Graph coloring is a way of coloring the vertices  of a graph such that no two
adjacent vertices are of the same color and the number of colors k is minimized

#### Instances

We are using the same instances as for CBTSP

## Approaches

### GA

For genetic algorithms, we use different parameter settings such as:
* population size
* number of generations
* mutation rate
* tournament size (for selection of parents)

Initial state is a random state.

Fitness function:
* CBTSP: the sum of total distances
* Graph coloring: 50 * num_of_colors + 100 * num_of_conflicts

## Setup and running

This project uses python 3.7, so you need to have it installed in order
to run the scripts.
You can find latest python releases here: https://www.python.org/downloads/


In order to install needed packages, run:

```bash
pip install -r requirements.txt
```

Everything is run from the main directory (som-exercise2), so in order to run the scripts do:

```bash
$ EXPORT PYTHONPATH=.
$ python usecases/genetic_algorithms/genetic_algorithms.py -p tsp -i 0010 -r 10
$ python usecases/ant_colony_optimizations/aco.py -p tsp -i 0010 -r 10
```

This will run the TSP on the instance 0010, and will repeat for 10 times. The graph coloring
can be run on the same way with GA-s by passing `graph_coloring` in the -p argument.

Default values are: solve tsp for all instances, by doing 10 runs per instance.

In order to try/add new parameters for GA, just modify the TESTING_PARAMETERS variable,
which looks something like this:

```
TESTING_PARAMETERS = [{
        'POPULATION_SIZE': 50,
        'NUMBER_OF_GENERATIONS': 200,
        'MUTATION_RATE': 0.05,
        'TOURNAMENT_SIZE': 10
    }]
```

Same setting is available for ACO-s as well, where parameters can be changed on:

```
TESTING_PARAMETERS = [{
        'ANT_COUNT': 200,
        'GENERATIONS': 100,
        'ALPHA': 0.5,
        'BETA': 8.0,
        'RHO': 0.5,
        'Q': 10,
        'STRATEGY': 1,
    }]
```


Running the script will save all results as xlsx files under `experiments` directory, with
the following naming convention (depending on what you run):

```
genetic_algorithm_tsp_{current timestamp}.xlsx, or
genetic_algorithm_graph_coloring_{current timestamp}.xlsx, or
aco_tsp_{current timestamp}.xlsx, or
aco_graph_coloring_{current timestamp}.xlsx
```
